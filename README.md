# Intern Test Umbala

### Lời giải đề CTDL test intern tại Umbala Blockchain Network </br>

- Đề test có 2 câu, ở đây chỉ giải câu 2 do câu 1 khá đơn giản
- Câu 2 (unsolving): Xoá node trùng trong 1 unsorted linked list, output là linked list chứa node trùng đã bị xoá <br/>
Ví dụ: <br/>
INPUT 12,11,12,21,41,43 <br/>
OUTPUT 12,11,21,41,43 <br/>
- Ý tưởng: dùng 2 hoặc 3 biến index để loop và tìm những node trùng nào cần xoá, thực hiện lệnh Remove. Chú ý các testcase